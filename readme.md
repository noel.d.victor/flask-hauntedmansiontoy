# The Haunted Mansion Text Browser Game

A practice project for me to get more comfortable with flask in a fun way.

Its a toy demo.

# Screenshot
![screenshot](screenshot.png)

# Setup
- you'll need docker and docker-compose
- then just cd into the dir
- run `docker-compose build`
- then  `docker-compose up`
- it should run on http://localhost:5000/game

# How to play
Just type an action from the possible actions and press enter.


# Flask Stuff
export FLASK_DEBUG=1
- this will launch the debugger similar to django debugger but you will also get a live debugger possible very dangerous because an attacker can get remote access

