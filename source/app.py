from flask import Flask
from flask import render_template
from flask import request
from flask import session
from flask import redirect
from flask import url_for, escape

import game_code as game_code

app = Flask(__name__)


@app.route('/')
def start_page():
    context = {}
    context['greeting'] = "Welcome to The Haunted Mansion Minigame!"
    context['message'] = "have fun!"
    return render_template("bootstrap_index.html",
                           context=context
                           )



@app.route('/game_start')
def game_start():
    # setup the session with starting values
    session['room_name'] = "Porch"
    return redirect(url_for('game'))


@app.route('/game', methods=["GET", "POST"])
def game():
    room_name = session.get('room_name')
    if request.method == "GET":
        if room_name:
            room = game_code.load_room(room_name)
            return render_template(
                'bootstrap_show_room.html',
                room=room
            )
    else:
        action = request.form.get('action')

        if room_name and action:
            room = game_code.load_room(room_name)
            next_room = room.go(action)

            if not next_room:
                session['room_name'] = game_code.name_room(room)
            else:
                session['room_name'] = game_code.name_room(next_room)
        return redirect(url_for("game"))


# YOU SHOULD CHANGE THIS IF YOU PUT ON THE INTERNET
# If you decide to run this on productions move it to an env
app.secret_key = 'N0Vv38j/3yX R~XHH!jmN]LWX/,?RT'

if __name__ == "__main__":
    # needed for docker
    # The web server running in your docker container is listening
    # for connections on port 5000 of the loopback network interface (127.0.0.1)
    # As such this web server will only respond to http requests originating from that container itself.
    #
    # In order for the web server to accept connections originating from
    # outside of the container you need to have it bind to the 0.0.0.0 IP address.
    app.run(debug=True, host='0.0.0.0')
