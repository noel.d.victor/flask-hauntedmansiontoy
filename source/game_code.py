active_rooms = {}


class Room(object):

    def __init__(self, name, description, image=None):
        self.name = name
        self.description = description
        self.image = image
        self.paths = {}
        active_rooms.update(
            {self.name: self}
        )

    def go(self, direction):
        return self.paths.get(direction, None)

    def add_paths(self, paths):
        self.paths.update(paths)

    def get_queries(self):
        return ", ".join(self.paths.keys())


porch = Room("Porch",
             """
             Welcome to Ghostly Manor!\n
             You are waiting outside.
             """,
             "/img/outside.jpg"

             )

entrance = Room("Entrance",
                """
                The door open by itself mysteriously.\n
                You are now in the entrance.
                """,
                "/img/enter.jpg"
                )

living_room = Room("Living Room",
                   """
                   You smell the odd combination of sulfur and lilac from the fire place. You stand in the living room feeling an odd chill.
                   """,
                   "/img/lvroom.jpg"
                   )
haunted_kitchen = Room("The Kitchen",
                       """
                       You walked into the kitchen.  You see an odd smattering of reddish brown stains everywhere. You also feel a strong chill and the faint sound of laughter.
                       """,
                       "/img/kit.jpg"
                       )

the_end_room = Room("The End",
                    """
                    You wisely left ghostly manor before spooky happenings occurred. Congrats for surviving!
                    
                    """,
                    "/img/win.jpg"

                    )

death = Room("Death!",
             """
             A knife materialized from the air, and stabbed you in the heart.
             You died !
             """,
             "/img/dead.jpg"
             )

porch.add_paths(
    {
        "enter": entrance,
        "leave": the_end_room
    }
)

entrance.add_paths(
    {
        "walk more": living_room,
        "go back": porch
    }
)

living_room.add_paths(
    {
        "walk more": haunted_kitchen,
        "go back": entrance
    }
)

haunted_kitchen.add_paths(
    {
        "stick around": death,
        "go back": living_room
    }
)

START = porch.name


def load_room(name):
    return active_rooms.get(name)


def name_room(room):
    for key, value in active_rooms.items():
        if value == room:
            return key
